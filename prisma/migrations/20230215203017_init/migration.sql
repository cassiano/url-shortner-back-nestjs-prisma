-- CreateTable
CREATE TABLE "ShortUrl" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "uniqueId" TEXT NOT NULL,
    "url" TEXT NOT NULL,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL
);

-- CreateTable
CREATE TABLE "ShortUrlVisit" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "userAgent" TEXT NOT NULL,
    "ip" TEXT NOT NULL,
    "shortUrlId" INTEGER NOT NULL,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    CONSTRAINT "ShortUrlVisit_shortUrlId_fkey" FOREIGN KEY ("shortUrlId") REFERENCES "ShortUrl" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);

-- CreateIndex
CREATE UNIQUE INDEX "ShortUrl_uniqueId_key" ON "ShortUrl"("uniqueId");

-- CreateIndex
CREATE INDEX "ShortUrlVisit_shortUrlId_idx" ON "ShortUrlVisit"("shortUrlId");
