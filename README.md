<!-- https://dev.to/majiyd/nestjs-x-prisma-made-for-each-other-2ad7 -->

# Install

npm i -g @nestjs/cli
nest new back-nestjs-prisma --strict
cd back-nestjs-prisma
code .
<modify port to 3001 in main.ts>
npm run start:dev
npm install prisma @prisma/client sqlite3
npm install -g prisma-repl
<add cors to main.ts>
nest g service short_url --no-spec
nest g controller short_url --no-spec
<copy dtos folder from another project>
npx prisma init
<copy schema.prisma contents from another project>
<copy DATABASE_URL env_var to .env>
npx prisma migrate dev --name init
open prisma/development.sqlite
npx prisma
npx prisma format
npx prisma validate
npx prisma generate
<copy prisma.service.ts from another project>
<add PrismaService to providers in to app.module.ts>
npm i class-transformer
<copy entities folder from another project>
npm run start:dev

# REPL

prisma-repl --verbose

> a = await db.shortUrl.findMany({ include: { visits: true } })
> a[0].visits.length

> b = await db.shortUrlVisit.findMany({ include: { shortUrl: true } })
> b[0].shortUrl

> c = await db.shortUrl.findMany({ include: { \_count: { select: { visits: true } } } })
> c[0].\_count.visits
