import { Injectable } from '@nestjs/common';
import { ShortUrl } from '@prisma/client';
import { PrismaService } from '../prisma.service';
import { CreateShortUrlDto } from './dto/create-short-url.dto';

export type ShortUrlStats = {
  shortUrls: {
    generated: number;
    visited: number;
  };
};

@Injectable()
export class ShortUrlService {
  constructor(private prisma: PrismaService) {}

  create(shortUrlData: CreateShortUrlDto) {
    return this.prisma.shortUrl.create({
      data: {
        ...shortUrlData,
        uniqueId: crypto.randomUUID(),
      },
    });
  }

  list() {
    return this.prisma.shortUrl.findMany({
      orderBy: { updatedAt: 'desc' },
      include: {
        _count: {
          select: { visits: true },
        },
      },
    });
  }

  async getStats(): Promise<ShortUrlStats> {
    const generated = await this.prisma.shortUrl.count();
    const visited = await this.prisma.shortUrlVisit.count();

    return {
      shortUrls: { generated, visited },
    };
  }

  find(id: number) {
    return this.findOneBy({ id });
  }

  destroy(id: number) {
    return this.prisma.shortUrl.delete({ where: { id } });
  }

  async addVisit(uniqueId: string, ip: string, userAgent: string) {
    const shortUrl = await this.findOneBy({ uniqueId });

    await this.prisma.shortUrlVisit.create({
      data: {
        shortUrlId: shortUrl.id,
        userAgent,
        ip,
      },
    });

    return shortUrl.url;
  }

  private findOneBy(attrs: Partial<ShortUrl>) {
    return this.prisma.shortUrl.findFirstOrThrow({
      where: { ...attrs },
      include: {
        _count: {
          select: { visits: true },
        },
      },
    });
  }
}
