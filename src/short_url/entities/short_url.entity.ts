import { Exclude, Expose } from 'class-transformer';

// https://docs.nestjs.com/techniques/serialization
export class ShortUrlEntity {
  id: number;
  uniqueId: string;

  @Expose()
  get links(): { visit: string; self: string } {
    return {
      visit: `${this.host}/api/short_urls/${this.uniqueId}/visit`,
      self: `${this.host}/api/short_urls/${this.id}`,
    };
  }

  @Expose()
  get visited(): number {
    return this._count?.visits ?? 0;
  }

  @Exclude()
  _count?: { visits: number };

  @Exclude()
  url: string;

  @Exclude()
  createdAt: Date;

  @Exclude()
  updatedAt: Date;

  constructor(partial: Partial<ShortUrlEntity>, private host: string) {
    Object.assign(this, partial);
  }
}
