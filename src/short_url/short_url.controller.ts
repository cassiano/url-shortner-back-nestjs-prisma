import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Redirect,
  Req,
  UseInterceptors,
} from '@nestjs/common';
import { Request } from 'express';
import { CreateShortUrlDto } from './dto/create-short-url.dto';
import { ShortUrlEntity } from './entities/short_url.entity';
import { ShortUrlService } from './short_url.service';

@Controller('api/short_urls')
@UseInterceptors(ClassSerializerInterceptor)
export class ShortUrlController {
  constructor(private readonly shortUrlService: ShortUrlService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  async create(
    @Body() createShortUrlDto: CreateShortUrlDto,
    @Req()
    req: Request,
  ) {
    return new ShortUrlEntity(
      await this.shortUrlService.create(createShortUrlDto),
      this.getHostWithPort(req),
    );
  }

  @Get()
  async index(
    @Req()
    req: Request,
  ) {
    const hostWithPort = this.getHostWithPort(req);

    return (await this.shortUrlService.list()).map(
      (shortUrl) => new ShortUrlEntity(shortUrl, hostWithPort),
    );
  }

  @Get('stats')
  stats() {
    return this.shortUrlService.getStats();
  }

  @Get(':id')
  async show(
    @Param('id') id: string,
    @Req()
    req: Request,
  ) {
    return new ShortUrlEntity(
      await this.shortUrlService.find(parseInt(id)),
      this.getHostWithPort(req),
    );
  }

  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  destroy(@Param('id') id: string) {
    return this.shortUrlService.destroy(parseInt(id));
  }

  @Get(':uniqueId/visit')
  @Redirect()
  async visit(
    @Param('uniqueId') uniqueId: string,
    @Req()
    req: Request,
  ) {
    const url: string = await this.shortUrlService.addVisit(
      uniqueId,
      req.ip,
      req.get('user-agent') ?? '(user agent not detected)',
    );

    return { url, statusCode: HttpStatus.FOUND };
  }

  // https://expressjs.com/en/api.html#req
  private getHostWithPort(req: Request) {
    return `${req.protocol}://${req.get('Host')}`;
  }
}
