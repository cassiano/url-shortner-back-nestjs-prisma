import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ShortUrlService } from './short_url/short_url.service';
import { ShortUrlController } from './short_url/short_url.controller';
import { PrismaService } from './prisma.service';
import { ShortUrlModule } from './short_url/short_url.module';

@Module({
  imports: [ShortUrlModule],
  controllers: [AppController, ShortUrlController],
  providers: [AppService, ShortUrlService, PrismaService],
})
export class AppModule {}
